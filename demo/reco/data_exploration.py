# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.6
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # Data exploration
#
# First we may need to install the used packages: Numpy, Pandas. (Pyspark is already installed)

# %pip install numpy pandas

# At first we start to import all necessary packages and functions.
# Then we start a spark session with a defined number of executers, cores, memory, etc. 
# We also add some s3 credentials so that we are allowed to read from s3 and are adding additional java dependencies which are needed so that spark can read from the s3 storage.
# Executing the next cell can take a while. So just execute it and keep calm!

# +
# Spark stuff
import pyspark
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.recommendation import ALS
from pyspark.sql import Row
from pyspark.sql.window import Window
from pyspark.sql.types import *
import pyspark.sql.functions as F
from pyspark.sql.functions import rand

# other packages
import numpy as np
import pandas as pd
import os
import json

spark = pyspark.sql.SparkSession\
       .builder\
       .appName("reco_data_exploration")\
       .config("spark.dynamicAllocation.maxExecutors", "6")\
       .config("spark.driver.cores", "8")\
       .config("spark.driver.memory", "16g")\
       .config("spark.executor.cores", "8")\
       .config("spark.executor.memory", "16g")\
       .config("spark.driver.maxResultSize", "2g")\
       .config("fs.s3a.aws.credentials.provider", "org.apache.hadoop.fs.s3a.AnonymousAWSCredentialsProvider")\
       .config("spark.jars.packages", "com.amazonaws:aws-java-sdk-pom:1.11.498,org.apache.hadoop:hadoop-aws:2.8.5")\
       .getOrCreate()
# -

# We save the data loaded from amazon s3 to our hdfs. This reduces the latency and we are able to make some processing on the data without it taking to much time.

testDf = spark.read.parquet("s3a://amazon-reviews-pds/parquet/product_category=Books")
testDf.write.mode("overwrite").parquet("/tiki/demo/reco/books")

# Now we load the data again, but this tome from hdfs. Otherwise the data would be read from s3 everytime we would make some processing with spark

amazonReviewDf = spark.read.parquet("/tiki/demo/reco/books")

# Now lets have a look at the data.  
# We will have a look at the schema:

amazonReviewDf.printSchema()

# Lets investigate some titles with "Harry potter" in their name!

amazonReviewDf.filter(F.col("product_title").contains("Harry Potter")).show(10, False)

# # Recommendation with ALS (Alternating Least Squares)  
#
# Now that we are able to read, write and investigate data, we want to recommend books for users depending on their already bought books.  
# But before we can start recommendation, we need to preprocess ou data:
#
# ## Preprocessing
#
# For that we start to define some variables:

min_user_interactions = 10           # <- We only can make recommendations for users that have bought some books
min_item_interaction = 10            # <- We only can make recommentations on books that have been bought some times
original_product_col = "product_id"
product_col = "product_id_int"
costumer_col = "customer_id"
rating_col = "star_rating"

# create unique integer id, since the used ID is in a bad format

product_id_mapping_df = amazonReviewDf.select(original_product_col)\
    .distinct()\
    .withColumn(product_col, F.row_number().over(Window.orderBy(F.desc(original_product_col))).cast("integer"))\
    .cache()

# Clean the data from bad rows. For example if (in any case) a rating is greater than 5, then something went wrong.

correctTypeDf = amazonReviewDf\
    .join(product_id_mapping_df, on=original_product_col)\
    .select(F.col(costumer_col).cast("integer"), F.col(product_col), F.col(rating_col))\
    .filter(F.col(product_col).isNull()==False)\
    .filter(F.col(costumer_col).isNull()==False)\
    .filter(F.col(rating_col)<=5)

# At the steps before duplicates can be be produced. So lets remove them

correctTypeDf = correctTypeDf.dropDuplicates()

# Remove products and users that are statistically not relevant

out_filtered_products = correctTypeDf.groupby(product_col).count().filter(F.col("count")>=min_item_interaction).select(product_col)
out_filtered_users = correctTypeDf.groupby(costumer_col).count().filter(F.col("count")>=min_user_interactions).select(costumer_col)
filteredDf = correctTypeDf\
    .join(out_filtered_products, on=product_col)\
    .join(out_filtered_users, on=costumer_col )\
    .cache()


# ## Model training and testing
#
# Finally we can start training our ALS model. We make a train-test split on the data and feed the trainings data to the ALS algorithm

(training, test) = filteredDf.randomSplit([0.8, 0.2])

# Build the recommendation model using ALS on the training data.  
# Note we set cold start strategy to 'drop' to ensure we don't get NaN evaluation metrics

als = ALS(rank = 20, maxIter=8, regParam=0.5, userCol=costumer_col, itemCol=product_col, ratingCol=rating_col,
          coldStartStrategy="drop")
model = als.fit(training)


# This was the whole magic. Now we should test the quality of our model. Therefor we compute the RMSE on the tes data

predictions = model.transform(test)
evaluator = RegressionEvaluator(metricName="rmse", labelCol="star_rating",
                                predictionCol="prediction")
rmse = evaluator.evaluate(predictions)
print("Root-mean-square error = " + str(rmse))

predictions.show()

# Ok, jea not bad, but whats the meaning of the result?  
#   
# We predicted for each user what his rating would be for each book. The above result shows Our mean squared error on this prediction.  
# We can now use the vector of all predicted books and select the top 5 products.

# ## Make predictions for known users with spark ALS
#
# From our test data we select a random user_id

random_user = test.select("customer_id").distinct().orderBy(rand()).limit(1).collect()[0]["customer_id"]

# and predict its top 5 recommended books

to_predict_users = spark.createDataFrame(Row({costumer_col: random_user}))
model.recommendForUserSubset(to_predict_users, 5).show(5,False)

# In the recommendation we now see 5 entries. Each entry contains a product_id and the predicted stars.

# ---
# ## Make predictions for new users
#
# Now we are able to make predictions for already known user. However if a new user appears, we don't have that users matrix entry. For that reason we have to generate a matrix row for a new user.  
# It is clear that a new user should come with some products he/she already rated. Otherwise a recommendation wouldn't make much sense.  
# From the few ratings the new user will come with we need to calculate the reduced row by:  
# > (userVector * featureMatrix) * featureMatrix^T
#
# - The userVector contains all ratings of the user to all Books that exist (almost all wil be zero).  
# - By multiplying this vector with the featureMatrix we generate a new reduced Vector for that user. This new vector is now comparable to a row in the userMatrix.  
# - Finally we can compute the rating to each other book by multiplying the reduced vector to the featureMatrix.  
#   
# At first we extract some matrices and vectors from the model so that we can use it with numpy.

# +
# create dictionary to map original product_id_ with product_id_int
product_mapping_collected = product_id_mapping_df.collect()
original_product_idx_mapping = {x[product_col]:x[original_product_col] for x in product_mapping_collected}

## load item latent factors matrix from spark to numpy arrays
product_features_df = model.itemFactors
product_features_collected = product_features_df.collect()

# load itemFactor data into lists
product_features_keys = [x["id"] for x in product_features_collected]
product_features_vals = [x["features"] for x in product_features_collected]

# create dictionary to map original product_id with idx in item_matrix
idx_for_product_id = {original_product_idx_mapping[x]:idx for idx, x in enumerate(product_features_keys)}
# inverse dictionary to get original product_id for idx
product_id_for_idx = {v: k for k, v in idx_for_product_id.items()}
# create numpy product feature matrix 
feature_matrix = np.matrix(np.asarray(product_features_vals))
# -

# Save feature results to local pvc so that we can load it later for webservice predictions

# +
# save paths
save_path = os.path.join(os.getcwd(), 'webservice_model')
feature_matrix_path = os.path.join(save_path, "feature_matrix.npy")
idx_for_product_id_path = os.path.join(save_path, "idx_for_product_id.json")
product_id_for_idx_path = os.path.join(save_path, "product_id_for_idx.json")

# save matrices and dictionary to save folder
np.save(feature_matrix_path,feature_matrix)
# save product_id_for_idx and idx_for_product_id dictionaries
json.dump(idx_for_product_id, open(idx_for_product_id_path,'w'))
json.dump(product_id_for_idx, open(product_id_for_idx_path,'w'))
# -

# load all necessary stuff for web service predictions
feature_matrix = np.load(feature_matrix_path)
idx_for_product_id = json.load(open(idx_for_product_id_path))
product_id_for_idx = json.load(open(product_id_for_idx_path))


# +
# function to set ratings in a full user vector
def set_rating(full_user_vector, product_id, rating):
    try:
        idx = idx_for_product_id[product_id]
        full_user_vector.itemset(idx, rating)
    except:
        pass

def make_predictions_for_ratings(rating_dict, top_n=10):
    full_user_vector = np.zeros(len(product_id_for_idx.keys()))
    for k,v in rating_dict.items():
        set_rating(full_user_vector, k, v)
        
    
    recommendations = np.dot(np.dot(full_user_vector,feature_matrix),feature_matrix.T)
    print(recommendations.shape)
    top_recommendations = np.argpartition(recommendations, -top_n)[-top_n:]
    # top ratings would be computed with: top_ratings = list(np.sort(recommendations)[:,-top_n:].flat)
    return [product_id_for_idx[str(idx)] for idx in top_recommendations]
# -

# ## Test predictions
# Make some exsample predictions with some example users

amazonReviewDf.filter(F.col("product_title").contains("Veggie")).show(10, False)

test_event = {"043935806X": 5, "0439785960":   4, "0439139597":5, "B00ES28WBA":5}
sample_predictions = make_predictions_for_ratings(test_event)

test_event = {"1606906607": 5, "R2JY54UKC7S0Y9":   5, "R2MGXYV748UDGM":2}
sample_predictions = make_predictions_for_ratings(test_event)

str(sample_predictions)

# ### show titles to the products

product_lookup_df = amazonReviewDf.select(original_product_col, "product_title").distinct().cache()


product_lookup_df.filter(F.col(original_product_col).isin(sample_predictions)).show(10, False)


