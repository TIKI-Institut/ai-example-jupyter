import ai_common
# packages
import numpy as np
import os
import json
from flask import Flask, jsonify, request


# load all necessary stuff for web service predictions
result_path="/opt/app/data"
feature_matrix_path = os.path.join(result_path, "feature_matrix.npy")
idx_for_product_id_path = os.path.join(result_path, "idx_for_product_id.json")
product_id_for_idx_path = os.path.join(result_path, "product_id_for_idx.json")
if os.path.isfile(feature_matrix_path):
    feature_matrix = np.load(feature_matrix_path)
if os.path.isfile(idx_for_product_id_path):
    idx_for_product_id = json.load(open(idx_for_product_id_path))
if os.path.isfile(product_id_for_idx_path):
    product_id_for_idx = json.load(open(product_id_for_idx_path))
    
    
# function to set ratings in a full user vector
def set_rating(full_user_vector, product_id, rating):
    try:
        idx = idx_for_product_id[product_id]
        full_user_vector.itemset(idx, rating)
    except:
        pass

def make_predictions_for_ratings(rating_dict={"043935806X": 5, "0439785960":   4, "0439139597":5, "B00ES28WBA":5}, top_n=10):   
    full_user_vector = np.zeros(len(product_id_for_idx.keys()))
    for k,v in rating_dict.items():
        set_rating(full_user_vector, k, v)
        
    recommendations = np.dot(np.dot(full_user_vector,feature_matrix),feature_matrix.T)
    top_recommendations = np.argpartition(recommendations, -top_n)[-top_n:]
    # top ratings would be computed with: top_ratings = list(np.sort(recommendations)[:,-top_n:].flat)
    return [product_id_for_idx[str(idx)] for idx in top_recommendations]
# -

@ai_common.decorators.web_resource('/open', None)
def open_endpoint():
    all_args = request.args.lists()
    if not all_args is None:
        default_recommendations = make_predictions_for_ratings()
        return "No args given, default recommendations are, %s" % str(default_recommendations)
    else:
        recommendations = make_predictions_for_ratings(jsonify(all_args))
        return "Recommendations for given ratings are, %s" % str(recommendations)
    
@ai_common.decorators.web_resource('/secure', ["infer"])
def secure_endpoint():
    all_args = request.args.lists()
    if not all_args is None:
        default_recommendations = make_predictions_for_ratings()
        return "No args given, default recommendations are, %s" % str(default_recommendations)
    else:
        recommendations = make_predictions_for_ratings(jsonify(all_args))
        return "Recommendations for given ratings are, %s" % str(recommendations)
    
@ai_common.decorators.web_resource('/secure-json', ["infer"], ["POST"])
def secure_endpoint():
    
    all_args = request.get_json()
    if all_args is None:
        default_recommendations = make_predictions_for_ratings()
        return "No args given, default recommendations are, %s" % str(default_recommendations)
    else:
        recommendations = make_predictions_for_ratings(all_args)
        return "Recommendations for given ratings are, %s" % str(recommendations)
    






