# Spark stuff
import pyspark
from pyspark.ml.recommendation import ALS
from pyspark.sql.window import Window
from pyspark.sql.types import *
import pyspark.sql.functions as F

# other packages
import numpy as np
import os
import json

#stuff for provisioning
import click
from ai_common.decorators import job

@job("train-model", None, short_help="Train a recommender model for using in webservice")
@click.option('--result_path', type=str, help='path where to save the final results')
def train_model(result_path="/opt/app/data"):
    spark = pyspark.sql.SparkSession\
           .builder\
           .appName("recommender_train")\
           .config("spark.dynamicAllocation.maxExecutors", "12")\
           .config("spark.executor.cores", "2")\
           .config("spark.executor.memory", "4g")\
           .config("spark.driver.maxResultSize", "2g")\
           .getOrCreate()
    # ## load data from hdfs 
    amazonReviewDf = spark.read.parquet("/tiki/demo/reco/books")

    # ## transform data for als
    min_user_interactions = 10
    min_item_interaction = 10
    original_product_col = "product_id"
    product_col = "product_id_int"
    costumer_col = "customer_id"
    rating_col = "star_rating"

    # create unique integer id
    product_id_mapping_df = amazonReviewDf.select(original_product_col)\
        .distinct()\
        .withColumn(product_col, F.row_number().over(Window.orderBy(F.desc(original_product_col))).cast("integer"))\
        .cache()

    correctTypeDf = amazonReviewDf\
        .join(product_id_mapping_df, on=original_product_col)\
        .select(F.col(costumer_col).cast("integer"), F.col(product_col), F.col(rating_col))\
        .filter(F.col(product_col).isNull()==False)\
        .filter(F.col(costumer_col).isNull()==False)\
        .filter(F.col(rating_col)<=5)

    #drop duplicates
    correctTypeDf = correctTypeDf.dropDuplicates()

    # filter out not so much used users and items
    out_filtered_products = correctTypeDf.groupby(product_col).count().filter(F.col("count")>=min_item_interaction).select(product_col)
    out_filtered_users = correctTypeDf.groupby(costumer_col).count().filter(F.col("count")>=min_user_interactions).select(costumer_col)
    filteredDf = correctTypeDf\
        .join(out_filtered_products, on=product_col)\
        .join(out_filtered_users, on=costumer_col )\
        .cache()

    # Build the recommendation model using ALS on the training data
    # Note we set cold start strategy to 'drop' to ensure we don't get NaN evaluation metrics
    als = ALS(rank = 20, maxIter=8, regParam=0.5, userCol=costumer_col, itemCol=product_col, ratingCol=rating_col, coldStartStrategy="drop")
    model = als.fit(filteredDf)

    # ## Make predictions for new users with numpy

    # create dictionary to map original product_id_ with product_id_int
    product_mapping_collected = product_id_mapping_df.collect()
    
    ## load item latent factors matrix from spark to numpy arrays
    product_features_df = model.itemFactors
    product_features_collected = product_features_df.collect()
    original_product_idx_mapping = {x[product_col]:x[original_product_col] for x in product_mapping_collected}

    # load itemFactor data into lists
    product_features_keys = [x["id"] for x in product_features_collected]
    product_features_vals = [x["features"] for x in product_features_collected]

    # create dictionary to map original product_id with idx in item_matrix
    idx_for_product_id = {original_product_idx_mapping[x]:idx for idx, x in enumerate(product_features_keys)}
    # inverse dictionary to get original product_id for idx
    product_id_for_idx = {v: k for k, v in idx_for_product_id.items()}
    # create numpy product feature matrix 
    feature_matrix = np.matrix(np.asarray(product_features_vals))
    # -

    ## save matrices and dictionary to save folder
    feature_matrix_path = os.path.join(result_path, "feature_matrix.npy")
    np.save(feature_matrix_path,feature_matrix)
    # save product_id_for_idx and idx_for_product_id dictionaries
    idx_for_product_id_path = os.path.join(result_path, "idx_for_product_id.json")
    json.dump(idx_for_product_id, open(idx_for_product_id_path,'w'))
    product_id_for_idx_path = os.path.join(result_path, "product_id_for_idx.json")
    json.dump(product_id_for_idx, open(product_id_for_idx_path,'w'))
    spark.stop()
    

