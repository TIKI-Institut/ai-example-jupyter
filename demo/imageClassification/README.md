# README
<p>
This folder contains coed to train a model on any image classification dataset. The main restriction is how the data
have to be structured. The data must be saved in the following way and uploaded to hdfs:
</p>

>── base_folder  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ├── class_1  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; │&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── **/*.pngs  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; │&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└── **/*.jpgs  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ├── class_2  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; │&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── **/*.pngs  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; │&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└── **/*.jpgs  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; └── class_3   
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── **/*.pngs  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└── **/*.jpgs


# Code adaption to new data

<p>
As soon as you have uploaded data to hdfs, you need to update the code:
</p>    

### src.helper

First add a new base folder for your data. There your data will be saved within the resource folder. Then you need to
add a new ```switcher``` key inside the ```dataset_folder_info``` function similar to the already implemented ones.  

### src.downloader

Add a new ```if``` statement. The comparision string must match the key in the ```switcher``` of
```src.helper.dataset_folder_info```. Copy the ```copy``` statement below the new ```if``` statement and adapt the hdfs
path as well as the key of ```dataset_folder_info``` variable.


# Execute

Start with the notebooks to download, investigate and train and evaluate a model. Just adapt the ```dataset``` variable
in each notebook to one of the supported datasets.  
The supported datasets can be found in the key of the ```switcher``` object in ```src.helper.dataset_folder_info```.

Execute the notebooks in the following order

<ol>
<lo>1. Import data</lo><br>
<lo>2. Investigate data</lo><br>
<lo>3. Train and evaluate model</lo><br>
</ol>