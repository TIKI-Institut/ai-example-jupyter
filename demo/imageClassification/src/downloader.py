import subprocess
from .helper import dataset_folder_info


def download_image_dataset(dataset_name: str):

    def copy(hdfs_directory, target_directory):
        subprocess.call(["hadoop", "fs", "-copyToLocal", hdfs_directory, target_directory])
        return 0
    
    if dataset_name == "deer_recognition" or dataset_name.lower() == "all":
        copy("hdfs:////tiki/image-recognition/labeled", dataset_folder_info("deer_recognition")["base_folder"])

    if dataset_name == "chessman" or dataset_name.lower() == "all":
        copy("hdfs:////tiki/chessman-classification", dataset_folder_info("chessman")["base_folder"])
        
    if dataset_name == "pcb" or dataset_name.lower() == "all":
        copy("hdfs:////tiki/image-classification/pcb_dataset", dataset_folder_info("pcb")["base_folder"])
