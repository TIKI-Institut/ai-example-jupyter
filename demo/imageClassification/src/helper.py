import os

# When you introduce a new dataset that shall work with this code, you should introduce a new variable
# with its name below and add the needed information to the "switcher" in "dataset_folder_info()"
deer_recognition_folder = "deerRecognition"
chessman_classification_folder = "chessmanClassification"
pcb_classification_folder = "pcb"


def dataset_folder_info(dataset: str):
    base_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "resources")
    
    def _get_classes(folder_name):
        if os.path.exists(folder_name):
            return os.listdir(folder_name)
        else:
            return "currently no classes"
    
    switcher = {
        "deer_recognition": {
            "base_folder": os.path.join(base_path, deer_recognition_folder),
            "classes": _get_classes(os.path.join(base_path, deer_recognition_folder))
        },
        "chessman": {
            "base_folder": os.path.join(base_path, chessman_classification_folder),
            "classes": _get_classes(os.path.join(base_path, chessman_classification_folder))
        },
        "pcb": {
            "base_folder": os.path.join(base_path, pcb_classification_folder),
            "classes": _get_classes(os.path.join(base_path, pcb_classification_folder))
        }
    }
    try:
        return switcher[dataset]
    except ValueError as err:
        print(f"{dataset} is no valid dataset. Select one of {switcher.keys()}") 
    