import torch
import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt


def predict(model, test_loader, device):
    # Disable autodiff for evaluation
    with torch.no_grad():
        predicted = []
        predicted_argmax = []
        labels = []
        probs = []

        # If set, move model to gpu and set to evaluation mode
        model = model.to(device).eval()

        # Main inference loop
        for idx, (data, label) in enumerate(test_loader):
            # If set, move data to gpu
            data = data.to(device)
            label = label.to(device)

            predicted.append(model(data))
            labels.append(label.cpu().numpy())

    # Transform prediction to binary class-predictions
    for tensor in predicted:
        for array in tensor:
            array = array.cpu()
            predicted_argmax.append(array.argmax())
            odds = np.exp(array.numpy())
            probs.append(odds / (1 + odds))

    return np.array(probs), np.array(predicted_argmax), np.concatenate(labels)


def show_some_image_predictions(model, test_data_loader, device, number_of_images_per_class=4):
    model.eval()

    classes = sorted(list(test_data_loader.dataset.labels_pd["label"].unique()))
    index_to_class = {v: k for k, v in test_data_loader.dataset.class_to_index.items()}
    df = test_data_loader.dataset.labels_pd.reset_index()

    fig, axs = plt.subplots(len(classes), number_of_images_per_class, figsize=(number_of_images_per_class*5, len(classes)*6))

    for i in range(len(classes)):
        temp_df = df[df["label"] == classes[i]]
        random_indexes = temp_df.sample(min(number_of_images_per_class, len(temp_df))).index.values

        for k, index in enumerate(random_indexes):
            # Make prediction
            data = test_data_loader.dataset.__getitem__(index)[0].to(device).unsqueeze(0)
            prediction = model(data)

            y_pred = prediction.argmax().item()
            odds = np.exp(prediction.detach().cpu())
            prob = (odds / (1 + odds))[0][y_pred]

            # Make plot
            image_path = df.iloc[index]["image_path"]
            current_axis = axs[i, k]
            current_axis.axis('Off')
            current_axis.title.set_text(f"Predicted class: {index_to_class[y_pred]}\nprobability: {prob}")
            img = mpimg.imread(image_path)
            current_axis.imshow(img)
    plt.show()