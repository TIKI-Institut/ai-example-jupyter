import torch
import torch.nn as nn
import torch.nn.functional as F


class CustomModel_Alpha(nn.Module):

    def __init__(self, num_classes, input_channels):
        super().__init__()
        self.conv1 = nn.Conv2d(input_channels, 6, kernel_size=5)
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv2 = nn.Conv2d(6, 16, kernel_size=5)
        self.lin1 = nn.Linear(16 * 5 * 5, 120)
        self.lin2 = nn.Linear(120, 84)
        self.lin3 = nn.Linear(84, num_classes)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.reshape(x.shape[0], -1)
        x = self.lin1(x)
        x = F.relu(x)
        x = self.lin2(x)
        x = F.relu(x)
        x = self.lin3(x)
        x = x.softmax(dim=1)
        return x
    
    
class CustomModel_Beta(nn.Module):
    
    def __init__(self, num_classes, input_channels):
        super().__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(input_channels, 8, kernel_size=(3,3), stride=(3,3), padding=0),
            nn.ReLU(inplace=True),
            nn.Conv2d(8, 16, kernel_size=(3,3), stride=(1,1), padding=0),
            nn.ReLU(inplace=True)
        )
        self.fc = nn.Linear(in_features=16*3*3, out_features=num_classes)
        
    def forward(self, x):
        x = self.conv(x)
        x = torch.flatten(x, start_dim=1, end_dim=-1)
        x = self.fc(x)
        x = x.softmax(dim=1)
        return x
        