from PIL import Image, ImageOps


def get_image_loaders() -> dict:
    return {
        "rgb": rgb_image_loader,
        "gray": gray_scale_image_loader
    }


# =============================================================================


def select_image_loader(image_loader_name: str):
    switcher = get_image_loaders()
    func = switcher.get(image_loader_name, "Invalid model type")
    return func


def rgb_image_loader(path: str):
    with open(path, 'rb') as f:
        img = Image.open(f)
        return img.convert('RGB')


def gray_scale_image_loader(path: str):
    with open(path, 'rb') as f:
        img = Image.open(f)
        return ImageOps.grayscale(img)
