from torchvision.transforms import transforms


def get_transformer() -> dict:
    return {
        "default": default_transformer,
        "optimized": optimized_transformer
    }


# =============================================================================


def select_transformer(transformer_name: str, image_size: tuple):
    switcher = get_transformer()
    func = switcher.get(transformer_name, "Invalid model type")
    return func(image_size)


def default_transformer(image_size: tuple):
    return general_transformer(image_size, [0.5, 0.5, 0.5], [0.5, 0.5, 0.5])


def optimized_transformer(image_size: tuple):
    return general_transformer(image_size, [0.485, 0.456, 0.406], [0.229, 0.224, 0.225])


def general_transformer(image_size: tuple, normalization_left, normalization_right):
    transformer = transforms.Compose([
        transforms.Resize(image_size),
        transforms.ToTensor(),
        transforms.Normalize(normalization_left, normalization_right)
    ])
    return transformer
