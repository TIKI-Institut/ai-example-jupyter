import torch


def train_model(model,
                optimizer,
                criterion,
                train_data_loader, 
                number_of_epochs,
                device="cpu"):
    
    epoch_loss = []
    epoch_acc = []

    model.to(device)
    criterion.to(device)

    for epoch in range(number_of_epochs):
        print(f"Epoch: {epoch}")
        running_loss = 0.0
        running_corrects = 0

        model.train()

        for inputs, labels in train_data_loader:
            inputs = inputs.to(device)
            labels = labels.to(device)

            optimizer.zero_grad()

            with torch.set_grad_enabled(True):
                outputs = model(inputs)

                loss = criterion(outputs, labels)
                _, preds = torch.max(outputs, 1)

                loss.backward()
                optimizer.step()

            running_loss += loss.item() * inputs.size(0)
            running_corrects += torch.sum(preds == labels.data)

        epoch_loss.append(running_loss / len(train_data_loader.dataset))
        epoch_acc.append(running_corrects / len(train_data_loader.dataset))
    
    return epoch_loss, epoch_acc
