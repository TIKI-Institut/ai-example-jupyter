from typing import Optional, Callable, Any, Tuple

import pandas as pd
from sklearn.utils.class_weight import compute_class_weight
from torch.utils.data import DataLoader, Dataset

from .custom_image_loader import select_image_loader, rgb_image_loader
from .custom_transformer import select_transformer


def get_train_test_validation_dataloader(labels_pd, 
                                         image_size, 
                                         batch_size: int,
                                         transformer_name: str = "default",
                                         image_loader_name: str = "rgb",
                                         with_class_weights: bool = True,
                                         shuffle: bool = True):
    train_data_loader, class_weights = get_dataloader(
        labels_pd[labels_pd["train_test_validation"] == "train"],
        image_size,
        batch_size,
        transformer_name,
        image_loader_name,
        with_class_weights,
        shuffle
    )

    test_data_loader, _ = get_dataloader(
        labels_pd[labels_pd["train_test_validation"] == "test"],
        image_size,
        batch_size,
        transformer_name,
        image_loader_name,
        with_class_weights,
        shuffle
    )

    validation_data_loader, _ = get_dataloader(
        labels_pd[labels_pd["train_test_validation"] == "validation"],
        image_size,
        batch_size,
        transformer_name,
        image_loader_name,
        with_class_weights,
        shuffle
    )
    
    return train_data_loader, test_data_loader, validation_data_loader, class_weights


def get_dataloader(labels_pd, 
                   image_size, 
                   batch_size: int,
                   transformer_name: str = "default",
                   image_loader_name: str = "rgb",
                   with_class_weights: bool = True,
                   shuffle: bool = True):
    transformer = select_transformer(transformer_name=transformer_name, image_size=image_size)

    dataset = CustomImageFolder(
        labels_pd=labels_pd,
        transform=transformer,
        loader=select_image_loader(image_loader_name)
    )

    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=shuffle, num_workers=10)

    # Compute weights manual, since all information are already known. Otherwise we would need to adapt
    # TrainTestImageFolder to implement functions to return correct number of classes and targets
    if with_class_weights:
        classes = sorted(labels_pd["label"].unique())
        class_weights = compute_class_weight("balanced", classes=classes, y=labels_pd["label"].values)
        class_weights = class_weights / sum(class_weights)

        return dataloader, class_weights
    else:
        return dataloader, []


class CustomImageFolder(Dataset):
    """
    Characterizes a dataset for PyTorch

    """

    def __init__(self,
                 labels_pd: pd.DataFrame,
                 transform: Optional[Callable] = None,
                 target_transform: Optional[Callable] = None,
                 loader: Callable[[str], Any] = rgb_image_loader):
        """Initialization"""
        self.transform = transform
        self.target_transform = target_transform
        self.labels_pd = labels_pd
        self.loader = loader
        self.classes = sorted(list(labels_pd["label"].unique()))
        self.class_to_index = {self.classes[i]: i for i in range(len(self.classes))}

    def __len__(self):
        """Denotes the total number of samples"""
        return len(self.labels_pd)

    def __getitem__(self, index) -> Tuple[Any, Any]:
        """Generates one sample of data"""
        # Select sample
        row = self.labels_pd.iloc[index]

        # Load data and get label
        path = row["image_path"]

        X = self.loader(path)
        y = self.class_to_index[row["label"]]

        if self.transform is not None:
            X = self.transform(X)
        if self.target_transform is not None:
            y = self.target_transform(y)

        return X, y
