import os 
import random

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 
import matplotlib.image as mpimg

from .helper import dataset_folder_info


class Describer:

    @staticmethod
    def describe_dataset(dataset: str):
        ds_info = dataset_folder_info(dataset)
        classes = ds_info["classes"]

        print(f"There are {len(classes)} different classes in the '{dataset}' dataset:")
        for i, c in enumerate(classes):
            n = len(os.listdir(os.path.join(ds_info["base_folder"], c)))
            print(f"   {i+1}. {c}   --> {n} images")

    # TODO add class title to each line
    @staticmethod
    def show_some_images(dataset: str, number_of_images_per_class: int = 4):
        ds_info = dataset_folder_info(dataset)
        classes = ds_info["classes"]

        fig, axs = plt.subplots(len(classes), number_of_images_per_class, figsize=(number_of_images_per_class*5, len(classes)*6))

        for i, c in enumerate(classes):
            image_paths = os.listdir(os.path.join(ds_info["base_folder"], c))

            # Remove unneccessary checkpoint files
            if ".ipynb_checkpoints" in image_paths:
                image_paths.remove(".ipynb_checkpoints")
            random.shuffle(image_paths)

            for j, k in enumerate(range(min(number_of_images_per_class, len(image_paths)))):
                current_axis = axs[i, j]
                current_axis.axis('Off')
                img = mpimg.imread(os.path.join(ds_info["base_folder"], c, image_paths[k]))
                current_axis.imshow(img)
        plt.show()

        
class Preparer:

    @staticmethod
    def get_train_test_validation_dataframe(dataset: str, train_test_validation: list = [0.8, 0.2, 0.00]):
        """
        Generate a pandas dataframe, with "label", "train-test-validation" and path information for each image.
        Example:
            image_path                 |  train_test_validation  |  label
            ---------------------------+-------------------------+---------
            total/path/to/00000199.jpg |  train                  |  Bishop
            total/path/to/00000110.jpg |  test                   |  Bishop
            total/path/to/00000112.jpg |  train                  |  King
        """
        ds_info = dataset_folder_info(dataset)
        
        df = pd.DataFrame()
        for c in ds_info["classes"]:

            img_files = os.listdir(os.path.join(ds_info["base_folder"], c))
            
            # Remove unneccessary checkpoint files
            if ".ipynb_checkpoints" in  img_files:
                img_files.remove(".ipynb_checkpoints")

            # Create train test split validation
            if np.sum(train_test_validation) != 1.0:
                train_test_validation = train_test_validation/np.sum(train_test_validation)
            number_of_trains = int(len(img_files) * float(train_test_validation[0]))
            number_of_tests = int(len(img_files) * float(train_test_validation[1]))
            number_of_validation = len(img_files) - (number_of_trains + number_of_tests)
            train_test_validation_list = ["train"] * number_of_trains + ["test"] * number_of_tests + ["validation"] * number_of_validation
            random.shuffle(train_test_validation_list)

            temp_df = pd.DataFrame({"image_path": img_files, "train_test_validation": train_test_validation_list})
            temp_df["image_path"] = temp_df["image_path"].apply(lambda r: os.path.join(ds_info["base_folder"], c, r))
            temp_df["label"] = c

            df = pd.concat([df, temp_df])
        return df
