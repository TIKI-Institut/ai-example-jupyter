# ai-example-jupyter

This project contains sample jupyter notebooks, which demonstarates how to use jupyter. There are also sample project for DSP provisioner.

Project structure:

* howtos - sample notebooks with basic usage and Spark, HDFS samples
* tests - notebooks with code snippets for testing
* demo - demo notebooks and projects
* tutorial - step for step manual for working with jupyter and DSP
